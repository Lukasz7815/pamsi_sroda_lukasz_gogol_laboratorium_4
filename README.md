#pragma once
#include "Drzewo.hh"

template <class Typ>
class DrzewoBinarne:public Drzewo<Typ>{
public:
	DrzewoBinarne(Typ dana):Drzewo<Typ>(new Node<Typ>(2,nullptr,dana),2){
		}

	void dodajElement(Typ dana,Node <Typ> * wsk){
		if(wsk->dana>dana){
			if(wsk->potomkowie[1]==nullptr)
				wsk->potomkowie[1]=new Node<Typ>(2,wsk,dana);
			else
				dodajElement(dana,wsk->potomkowie[1]);
		}else if(wsk->dana==dana){
			std::cout<<"Dana jest juz w drzewie";
		}
			else{
			if(wsk->potomkowie[0]==nullptr)
							wsk->potomkowie[0]=new Node<Typ>(2,wsk,dana);
			else
							dodajElement(dana,wsk->potomkowie[0]);
		}
	}


	void usunElement(Typ dana){
		usunElement(dana,this->getRoot());
	}
 void usunElement(Typ dana,Node <Typ> * wsk){
	 Node <Typ> * tmp=wsk->poprz;

	 if(dana==wsk->dana){
		 if(wsk->isLeaf()){
			 for(int i=0;i<tmp->iloscPotomow;i++){
				 if(tmp->potomkowie[i]==wsk){
					 delete wsk;
					 tmp->potomkowie[i]=nullptr;
				 }
			 }
		 }else{
			 std::cout<<"wezel nie jest lisciem\n";
		 }

	 }else if(dana<wsk->dana){
		 if(wsk->potomkowie[1]!=nullptr)
		 usunElement(dana,wsk->potomkowie[1]);
		 else
		 std::cout<<"Nie ma takiego elementu \n";
	 }else if(dana>wsk->dana){
		 if(wsk->potomkowie[0]!=nullptr)
		 usunElement(dana,wsk->potomkowie[0]);
		 else
			 std::cout<<"Nie ma takiego elementu \n";

	 }else{
		 std::cout<<"Nie ma takiego elementu \n";
	 }

 }



};